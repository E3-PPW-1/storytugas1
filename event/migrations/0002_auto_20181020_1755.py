# Generated by Django 2.1.1 on 2018-10-20 10:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(default='null', max_length=40)),
                ('password', models.CharField(default='null', max_length=40)),
                ('email', models.EmailField(default='null@gmail.com', max_length=254, unique=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Jadwal',
        ),
    ]
