from django.test import TestCase

# Create your tests here.
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Event
from .forms import EventCreationForm
from datetime import datetime

# Create your tests here.
# class SignUpPageUnitTest(TestCase):

#     def test_sign_up_url_is_exist(self):
#         response = Client().get('/event/signup/')
#         self.assertEqual(response.status_code, 200)

#     def test_sign_up_page_using_signup_template(self):
#         response = Client().get('/event/signup/')
#         self.assertTemplateUsed(response, 'signup.html')

#     def test_sign_up_page_content(self):
#         response = Client().get('/event/signup/')
#         html_response = response.content.decode('utf8')
#         self.assertIsNotNone(html_response)
#         self.assertIn('Formulir anggota', html_response)

class EventModelUnitTest(TestCase):

    def test_model_can_create_new_event(self):
        new_event = Event.objects.create(username="Usename", 
            email="email@gmail.com", password="inipassword" )
        counting_all_available_event = Event.objects.all().count()
        self.assertEqual(counting_all_available_event, 1)

    def test_model_not_create_invalid_username(self):
        #test melebihi max length
        test = "a" * 151

        new_event = Event.objects.create(username=test, 
            email="email@gmail.com", password="inipassword" )

        self.assertRaises(ValidationError, new_event.full_clean)

    def test_model_not_create_invalid_email(self):
        #test tidak menggunakan @
        test = "a"

        new_event = Event.objects.create(username="username", 
            email=test, password="inipassword" )
        self.assertRaises(ValidationError, new_event.full_clean)

    def test_model_not_create_invalid_password(self):
        #test melebihi max length
        test = "a" * 151
        new_event = Event.objects.create(username="username", 
            email="email@gmail.com", password=test )
        self.assertRaises(ValidationError, new_event.full_clean)
 

    def test_email_unique(self):
        new_event = Event.objects.create(username="Usename1", 
            email="email@gmail.com", password="inipassword1" )
        new_event_not_unique_email = Event.objects.create(username="Usename2", 
            email="email@gmail.com", password="inipassword2" )
        self.assertRaises(ValidationError, new_event_not_unique_email.full_clean)

    def test_username_unique(self):
        new_event = Event.objects.create(username="Usename1", 
            email="email@gmail.com", password="inipassword1" )
        new_event_not_unique_username = Event.objects.create(username="Usename2", 
            email="email@gmail.com", password="inipassword2" )
        self.assertRaises(ValidationError, new_event_not_unique_username.full_clean)

class EventFormUnitTest(TestCase):
    
    def test_validation_for_blank_items(self):
        form = EventCreationForm(data={'username':'', 
            'email':'', 'password1':'', 'password2':''})
        self.assertFalse(form.is_valid())  
        self.assertEqual(form.errors['username'],["This field is required."])    
        self.assertEqual(form.errors['email'],["This field is required."]) 
        self.assertEqual(form.errors['password1'],["This field is required."])   
        self.assertEqual(form.errors['password2'],["This field is required."])    

    def test_password_not_matched(self):
        form = EventCreationForm(data={'username':'akuadalah', 
            'email':'akuadalah@gmail.com','password1':'kucingkuadalah', 'password2':'akuadalah'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['password2'],["The two password fields didn't match."]) 