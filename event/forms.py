from django import forms
from .models import Data_Event

class NameForms(forms.ModelForm):

    username = forms.CharField(min_length=8, max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama pengguna",
        }
    ))

    email = forms.EmailField(max_length=30, widget=forms.EmailInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Email",
        }
    ))

    password1 = forms.CharField(min_length=8, max_length=150, widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Kata sandi",
        }
    ))

    password2 = forms.CharField(min_length=8, max_length=150, widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Konfirmasi kata sandi",
        }
    ))

    class Meta:
       model = Data_Event
       fields = ('username', 'email', 'password1', 'password2')