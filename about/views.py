from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Komentar
from member.models import Member
from .forms import TestimoniField


# Create your views here.

def about(request):
	response = {}
	form = TestimoniField
	#user = Member.objects.filter(request.user.username)
	if (request.method == 'POST'):
		form = TestimoniField(request.POST or None)	
		if (form.is_valid()):
			form.save()
	response['form'] = form
	
	testimoni = Komentar.objects.all()
	response['list'] = testimoni
	return render(request, 'about.html', response)