from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from time import time
from .views import about
from .models import Komentar
from .forms import TestimoniField

# Create your tests here.
class EventTest(TestCase):
    def test_about_url_exist(self):
        response = Client().get('/about/')
        self.assertEquals(response.status_code, 200)

    def test_about_using_index_func(self):
        found = resolve('/about/')
        self.assertEquals(found.func, about)

    def test_about_using_event_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_about_content(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('About', html_response)

class AboutModelTest(TestCase):
    def test_can_create_new_event(self):
        new_testimoni = Komentar.objects.create(waktu = timezone.now(),
                                            deskripsi = "Suatu deskripsi")
        counting_model = Komentar.objects.all().count()
        self.assertEqual(counting_model, 1)

class FormTest(TestCase):
    def test_valid_form(self):
        testimoni = Komentar.objects.create(waktu = timezone.now(),
                                           deskripsi = "Suatu deskripsi")
        data = {'waktu':testimoni.waktu, 'deskripsi':testimoni.deskripsi,}
        form = TestimoniField(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        testimoni = Komentar.objects.create(waktu = timezone.now(),
                                           deskripsi = "")
        data = {'waktu':testimoni.waktu, 'deskripsi':testimoni.deskripsi,}
        form = TestimoniField(data=data)
        self.assertFalse(form.is_valid())

# class PendaftarTest(TestCase):
#     def test_pendaftar_page_url_exist(self):
#         response = Client().get('/event_page/pendaftar/')
#         self.assertEquals(response.status_code, 200)

#     def test_pendaftar_using_index_func(self):
#         found = resolve('/event_page/pendaftar/')
#         self.assertEquals(found.func, pendaftar)

#     def test_news_page_using_event_template(self):
#         response = Client().get('/event_page/pendaftar/')
#         self.assertTemplateUsed(response, 'list_pendaftar.html')

#     def test_event_page_content(self):
#         response = Client().get('/event_page/pendaftar/')
#         html_response = response.content.decode('utf8')
#         self.assertIsNotNone(html_response)
#         self.assertIn('Pendaftar', html_response)