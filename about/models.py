from django.db import models
from member.models import Member

# Create your models here.
class Komentar(models.Model):
    deskripsi = models.TextField(max_length=300)
    waktu = models.DateTimeField(auto_now_add=True, editable=False)
    #nama_user = models.OneToOneField('self', Member)
    #nama_user = models.CharField(max_length = 30, default = None)
    def __str__(self):
        return self.deskripsi