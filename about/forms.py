from django import forms
from .models import Komentar

class TestimoniField(forms.ModelForm):
    deskripsi = forms.CharField(widget=forms.TextInput(
        attrs={
            'placeholder': 'Testimoni anda',
        }
    ))

    class Meta:
        model = Komentar
        fields = {'deskripsi'}