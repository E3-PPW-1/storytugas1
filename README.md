# Repository Tugas 2 PPW Kelompok E3
Fakultas Ilmu Komputer, Universitas Indonesia, Semester Gasal 2018/2019
***

## Anggota Kelompok

Berikut adalah anggota dari kelompok E3.  

1. Ahmad Fauzan Amirul Isnain - 1706979152  
   Bagian Tugas : Login with Google  
   App name     : member   

2. Muhammad Naufal Raihansyah - 1706043600    
   Bagian Tugas : Halaman About, Testimoni halaman about   
   App name 	:      

3. Rahmat Fadhilah - 1706074902   
   Bagian Tugas : Daftar List Event, Daftar ke suatu event   
   App name     :     

4. Saul Andre Lumban Gaol - 1706023555   
   Bagian Tugas : Daftar event yang pernah didaftarkan sebelumnya    
   App name     :    

## Pipeline Status

[![pipeline status](https://gitlab.com/E3-PPW-1/storytugas1/badges/master/pipeline.svg)](https://gitlab.com/E3-PPW-1/storytugas1/commits/master)

## Code Coverage Status

[![coverage report](https://gitlab.com/E3-PPW-1/storytugas1/badges/master/coverage.svg)](https://gitlab.com/E3-PPW-1/storytugas1/commits/master)

## Heroku Link

https://ppw-e3-1.herokuapp.com/

## Panduan Kontribusi Kelompok

1. Clone dari branch dev  
    `git clone -b dev https://gitlab.com/E3-PPW-1/storytugas1.git`

2. Jalankan cmd di folder git hasil clone, buat branch baru  
    `git branch <namabranch>`

3. Pindah ke branch yang sudah dibuat  
    `git checkout <namabranch>`

4. Lakukan pekerjaan

5. Push ke branch yang sudah dibuat  
    `git push origin <namabranch>`

6. Buat merge request ke dev jika sudah final