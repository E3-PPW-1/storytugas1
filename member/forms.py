from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Member
from datetime import date
from django.contrib.auth.forms import AuthenticationForm

class MemberCreationForm(UserCreationForm):

    fullname = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama lengkap",
        }
    ))

    username = forms.CharField(min_length=8, max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama pengguna",
        }
    ))

    email = forms.EmailField(max_length=30, widget=forms.EmailInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Email",
        }
    ))


    birthdate = forms.DateField(widget=forms.DateInput(
        attrs={
            'class': 'form-control textbox-n',
            'placeholder': 'Tanggal lahir',
            'type' : 'text', 
            'onfocus' : "(this.type='date')",
        }
    ))

    address = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Alamat",
        }
    ))
    
    password1 = forms.CharField(min_length=8, max_length=150, widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Kata sandi",
        }
    ))

    password2 = forms.CharField(min_length=8, max_length=150, widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Konfirmasi kata sandi",
        }
    ))

    class Meta(UserCreationForm.Meta):
       model = Member
       fields = ('fullname', 'username', 'email', 'birthdate', 'address', 'password1', 'password2')

    def clean_birthdate(self):
        try:
            birthdate = self.cleaned_data['birthdate']
        finally:
            if birthdate >= date.today():
                self.add_error('birthdate', 'Birthdate should be on past.')
            return birthdate

class MyLoginForm(AuthenticationForm):

    username = forms.CharField(min_length=8, max_length=150, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Nama pengguna",
        }
    ))

    password = forms.CharField(min_length=8, max_length=150, widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': "Kata sandi",
        }
    ))

