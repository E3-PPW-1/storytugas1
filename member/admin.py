from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import MemberCreationForm
from .models import Member
# Register your models here.

class CustomUserAdmin(UserAdmin):
    add_form = MemberCreationForm
    model = Member
    list_display = ['fullname', 'username', 'email', 'birthdate', 'address', 'password']

admin.site.register(Member, CustomUserAdmin)
