from django.urls import path
from . import views
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from .forms import MyLoginForm

urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('login/', auth_views.LoginView.as_view(template_name="registration/login.html", authentication_form=MyLoginForm), name="login"),
	path('logout/', auth_views.LogoutView.as_view()),
	path('check_login/', views.check_login)
]