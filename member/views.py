from .forms import MemberCreationForm
from django.urls import reverse_lazy
from django.views import generic
from django.http import JsonResponse

response = {}

class SignUp(generic.CreateView):
    form_class = MemberCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

def check_login(request):
	response['loggedIn'] = False
	if request.user.is_authenticated:
		response['loggedIn'] = True
	return JsonResponse(response)