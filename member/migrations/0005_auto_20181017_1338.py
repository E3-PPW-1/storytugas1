# Generated by Django 2.1.1 on 2018-10-17 06:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('member', '0004_auto_20181017_1337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='fullname',
            field=models.CharField(default='null', max_length=30),
        ),
    ]
