# Generated by Django 2.1.1 on 2018-10-16 23:58

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('member', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='adress',
            field=models.CharField(default='null@gmail.com', max_length=150),
        ),
        migrations.AddField(
            model_name='member',
            name='birthdate',
            field=models.DateField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='member',
            name='fullname',
            field=models.CharField(default='null', max_length=30),
        ),
        migrations.AlterField(
            model_name='member',
            name='email',
            field=models.EmailField(max_length=254),
        ),
    ]
