from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime
# Create your models here.

class Member(AbstractUser):
    fullname = models.CharField(default='null', max_length=40)
    email = models.EmailField(default="null@gmail.com", unique=True)
    birthdate = models.DateField(default=datetime.now)
    address = models.CharField(default='null', max_length=300)
    
    def __str__(self):
        return self.email
