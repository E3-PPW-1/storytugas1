from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Member
from .forms import MemberCreationForm
from datetime import date
import json

# Create your tests here.
class SignUpPageUnitTest(TestCase):

    def test_sign_up_url_is_exist(self):
        response = Client().get('/member/signup/')
        self.assertEqual(response.status_code, 200)

    def test_sign_up_page_using_signup_template(self):
        response = Client().get('/member/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_sign_up_page_content(self):
        response = Client().get('/member/signup/')
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Formulir anggota', html_response)

class LoginUnitTest(TestCase):

    def test_check_login_success(self):
        member = Member.objects.create(username='username')
        member.set_password('password')
        member.save()
        login = self.client.login(username='username', password='password')
        response = self.client.get('/member/check_login/')
        data = json.loads(response.content)
        self.assertEqual(data['loggedIn'], True)

    def test_check_not_login(self):
        response = self.client.get('/member/check_login/')
        data = json.loads(response.content)
        self.assertEqual(data['loggedIn'], False)


class MemberModelUnitTest(TestCase):

    def test_model_can_create_new_member(self):
        new_member = Member.objects.create(fullname="Nama", username="Usename", 
            email="email@gmail.com", birthdate='2000-12-12', address="Ini address", 
            password="inipassword" )
        counting_all_available_member = Member.objects.all().count()
        self.assertEqual(counting_all_available_member, 1)


    def test_model_not_create_invalid_fullname(self):
        #test melebihi max length
        test = "a" * 41

        new_member = Member.objects.create(fullname=test, username="Username", 
            email="email@gmail.com", birthdate='2000-12-12', address="Ini address", 
            password="inipassword" )

        self.assertRaises(ValidationError, new_member.full_clean)

    def test_model_not_create_invalid_username(self):
        #test melebihi max length
        test = "a" * 151

        new_member = Member.objects.create(fullname="Nama", username=test, 
            email="email@gmail.com", birthdate='2000-12-12', address="Ini address", 
            password="inipassword" )

        self.assertRaises(ValidationError, new_member.full_clean)

    def test_model_not_create_invalid_email(self):
        #test tidak menggunakan @
        test = "a"

        new_member = Member.objects.create(fullname="Nama", username="username", 
            email=test, birthdate='2000-12-12', address="Ini address", 
            password="inipassword" )
        self.assertRaises(ValidationError, new_member.full_clean)

    def test_model_not_create_invalid_birthdate(self):
        #test birthdate bukan berupa tanggal
        test = "0000-00-00"
        Error = ""
        try:
            new_member = Member.objects.create(fullname="Nama", username="username", 
                email="email@gmail.com", birthdate=test, address="Ini address", 
                password="inipassword")
        except:
            Error = "Error Occured"
        self.assertEqual(Error, "Error Occured")

    def test_model_not_create_invalid_address(self):
        #test melebihi max length
        test = "a"*301

        new_member = Member.objects.create(fullname="Nama", username="username", 
            email="email@gmail.com", birthdate='2000-12-12', address=test, 
            password="inipassword" )
        self.assertRaises(ValidationError, new_member.full_clean)

    def test_model_not_create_invalid_password(self):
        #test melebihi max length
        test = "a" * 151
        new_member = Member.objects.create(fullname="Nama", username="username", 
            email="email@gmail.com", birthdate='2000-12-12', address="Ini address", 
            password=test )
        self.assertRaises(ValidationError, new_member.full_clean)

    def test_member_str(self):
        new_member = Member(fullname="Nama", username="username", 
            email="email@gmail.com", birthdate='2000-12-12', address="Ini address", 
            password="inipassword" )
        self.assertEqual(str(new_member),'email@gmail.com')     

    def test_validation_unique_email(self):
        new_member = Member.objects.create(fullname="Nama1", username="Usename", 
            email="email@gmail.com", birthdate='2000-12-12', address="Ini address1", 
            password="inipassword1" )
        Error = ""
        try :
            new_member_not_unique_username = Member.objects.create(fullname="Nama2", username="CobaUserLain", 
                email="email@gmail.com", birthdate='2000-12-12', address="Ini address2", 
                password="inipassword2" )
        except IntegrityError:
            Error = "Error Occured"        
        self.assertEqual(Error, "Error Occured")

    def test_validation_unique_username(self):
        new_member = Member.objects.create(fullname="Nama1", username="Usename", 
            email="email@gmail.com", birthdate='2000-12-12', address="Ini address1", 
            password="inipassword1" )
        Error = ""
        try :
            new_member_not_unique_username = Member.objects.create(fullname="Nama2", username="Usename", 
                email="kucingemail@gmail.com", birthdate='2000-12-12', address="Ini address2", 
                password="inipassword2" )
        except IntegrityError:
            Error = "Error Occured"        
        self.assertEqual(Error, "Error Occured")

class MemberFormUnitTest(TestCase):
    
    def test_validation_for_blank_items(self):
        form = MemberCreationForm(data={'fullname':'', 'username':'', 
            'email':'', 'birthdate':'', 'address':'', 
            'password1':'', 'password2':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['fullname'],["This field is required."])    
        self.assertEqual(form.errors['username'],["This field is required."])    
        self.assertEqual(form.errors['email'],["This field is required."])    
        self.assertEqual(form.errors['birthdate'],["This field is required."])    
        self.assertEqual(form.errors['address'],["This field is required."])    
        self.assertEqual(form.errors['password1'],["This field is required."])   
        self.assertEqual(form.errors['password2'],["This field is required."])    

    def test_password_not_matched(self):
        form = MemberCreationForm(data={'fullname':'akuadalah', 'username':'akuadalah', 
            'email':'akuadalah@gmail.com', 'birthdate':'2000-12-12', 'address':'disini', 
            'password1':'kucingkuadalah', 'password2':'akuadalah'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['password2'],["The two password fields didn't match."]) 

    def test_password_similar_username(self):
        form = MemberCreationForm(data={'fullname':'akuadalah', 'username':'akuadalah', 
            'email':'akuadalah@gmail.com', 'birthdate':'2000-12-12', 'address':'disini', 
            'password1':'akuadalah', 'password2':'akuadalah'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['password2'],['The password is too similar to the username.'])

    def test_birthdate_should_be_on_past(self):
        form = MemberCreationForm(data={'fullname':'akuadalah', 'username':'akuadalah', 
            'email':'akuadalah@gmail.com', 'birthdate':date.today(), 'address':'disini', 
            'password1':'akuadalah', 'password2':'akuadalah'})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['birthdate'],['Birthdate should be on past.'])
