from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.
class LandingPageTest(TestCase):

    def test_landing_page_url_is_exist(self):
        response = Client().get('/landing_page/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/landing_page/')
        self.assertEqual(found.func, index)

    def test_landing_page_content(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Student Union', html_response)

    def test_landing_page_using_landing_page_template(self):
    	response = Client().get('/landing_page/')
    	self.assertTemplateUsed(response, 'landing_page.html')
