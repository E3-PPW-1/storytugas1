from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, news_detail
from .models import News
from datetime import datetime

# Create your tests here.

class NewsTest(TestCase):
    def test_news_page_url_exist(self):
        response = Client().get('/news/')
        self.assertEquals(response.status_code, 200)
    
    def test_news_using_index_func(self):
        found = resolve('/news/')
        self.assertEquals(found.func, index)

    def test_news_page_using_news_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news_page.html')
    
    

class NewsModelsUnitTest(TestCase):
    def test_model_can_create_new_news(self):
        new_news = News.objects.create(title="Berita 1", image="lala", 
                                abstract="Lorem ipsum dolor sit amet,consectetur adipiscing elit",
                                text="Lorem ipsum dolor sit amet, consectetur adipiscing elit", 
                                date=datetime.now())
        counting_model_content = News.objects.all().count()
        self.assertEqual(counting_model_content, 1)
