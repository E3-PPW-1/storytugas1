from django.urls import path
from .views import index, news_detail
from django.conf.urls import url

urlpatterns = [
    path('', index),
    url(r'^(?P<slug>[\w-]+)/$', news_detail, name="detail"),
]