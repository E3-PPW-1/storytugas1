from django.shortcuts import render
from .models import News
from django.http import HttpResponse

# Create your views here.
def index(request):
    newses = News.objects.all()
    return render(request, 'news_page.html', {'newses':newses})

def news_detail(request, slug):
    # return HttpResponse(slug)
    news = News.objects.get(slug=slug)
    return render(request, 'news_details.html', {'news_details':news})