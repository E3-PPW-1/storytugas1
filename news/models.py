from django.db import models
from datetime import datetime


# Create your models here.
class News(models.Model):
    title = models.CharField(default= 'null', max_length=75)
    slug = models.SlugField(default='null')
    image = models.TextField(default='https://fr.ubergizmo.com/wp-content/uploads/2012/11/rage-comic-testicule-cancer.jpg')
    abstract= models.TextField()
    text = models.TextField()
    date = models.DateField(default=datetime.now)

    def __str__(self):
        return self.title

