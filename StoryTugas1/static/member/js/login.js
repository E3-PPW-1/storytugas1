sessionStorage.loggedIn = false;

$(document).ready(function(){
	$(function(){
		$.ajax({
			url: "/member/check_login/",
			success: function(result) {
				if (result.loggedIn){
					sessionStorage.loggedIn = true;
					$('#modal-welcome').modal('show');
				} else {
					$('#modal-welcome').modal('hide');
				}
        	},
		});
	});
	
	$("body").on('DOMSubtreeModified', "#modal-welcome", function() {
  		var modalDisplay = $('#modal-welcome').css("display");
  		if (modalDisplay == "none" && sessionStorage.loggedIn){
  			$(location).attr('href',"/news");
  		}
	});
});