from django.db import models
# from event.models import Pengunjung
# Create your models here.

class Data_Event(models.Model):
    nama_kegiatan = models.CharField(max_length = 30)
    waktu_tanggal = models.DateTimeField('date')
    deskripsi_kegiatan = models.CharField(max_length = 60)

    # pengunjung = models.ManyToManyField(Pengunjung)

    class Meta:
        ordering = ('nama_kegiatan',)