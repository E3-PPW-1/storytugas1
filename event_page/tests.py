from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from time import time
from .views import event, pendaftar
from .models import Data_Event

# Create your tests here.
class EventTest(TestCase):
    def test_event_page_url_exist(self):
        response = Client().get('/event_page/')
        self.assertEquals(response.status_code, 200)

    def test_event_using_index_func(self):
        found = resolve('/event_page/')
        self.assertEquals(found.func, event)

    def test_event_page_using_event_template(self):
        response = Client().get('/event_page/')
        self.assertTemplateUsed(response, 'event_page.html')

    def test_event_page_content(self):
        response = Client().get('/event_page/')
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Kegiatan', html_response)

class EventModelTest(TestCase):
    def test_can_create_new_event(self):
        new_event = Data_Event.objects.create(nama_kegiatan = "Sesuatu",
                                            waktu_tanggal = timezone.now(),
                                            deskripsi_kegiatan = "Suatu deskripsi")
        counting_model = Data_Event.objects.all().count()
        self.assertEqual(counting_model, 1)

class PendaftarTest(TestCase):
    def test_pendaftar_page_url_exist(self):
        response = Client().get('/event_page/pendaftar/')
        self.assertEquals(response.status_code, 200)

    def test_pendaftar_using_index_func(self):
        found = resolve('/event_page/pendaftar/')
        self.assertEquals(found.func, pendaftar)

    def test_news_page_using_event_template(self):
        response = Client().get('/event_page/pendaftar/')
        self.assertTemplateUsed(response, 'list_pendaftar.html')

    def test_event_page_content(self):
        response = Client().get('/event_page/pendaftar/')
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Pendaftar', html_response)