from django.urls import path
from .views import event, pendaftar

urlpatterns = [
    path('', event),
    path('pendaftar/', pendaftar)
]