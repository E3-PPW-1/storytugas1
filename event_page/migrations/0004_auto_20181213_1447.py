# Generated by Django 2.1.1 on 2018-12-13 07:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event_page', '0003_auto_20181021_0038'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='data_event',
            options={'ordering': ('nama_kegiatan',)},
        ),
    ]
