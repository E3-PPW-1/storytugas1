from django.shortcuts import render
from .models import Data_Event
#from event.models import Event

# Create your views here.

def event(request):
	events = Data_Event.objects.all()
	return render(request, 'event_page.html', {'events': events})

def pendaftar(request):
	return render(request, 'list_pendaftar.html')
	
